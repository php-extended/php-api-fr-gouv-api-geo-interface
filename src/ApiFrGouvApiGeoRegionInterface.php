<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo;

use Stringable;

/**
 * ApiFrGouvApiGeoRegionInterface interface file.
 * 
 * This represents a region from the api.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvApiGeoRegionInterface extends Stringable
{
	
	/**
	 * Gets the code of the region.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the nom of the region.
	 * 
	 * @return string
	 */
	public function getNom() : string;
	
}
