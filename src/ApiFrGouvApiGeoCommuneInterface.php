<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo;

use PhpExtended\GeoJson\GeoJsonGeometryInterface;
use Stringable;

/**
 * ApiFrGouvApiGeoCommuneInterface interface file.
 * 
 * This represents a commune from the api.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvApiGeoCommuneInterface extends Stringable
{
	
	/**
	 * Gets the code of the commune.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the code departement of the commune.
	 * 
	 * @return string
	 */
	public function getCodeDepartement() : string;
	
	/**
	 * Gets the code region of the commune.
	 * 
	 * @return string
	 */
	public function getCodeRegion() : string;
	
	/**
	 * Gets the nom of the commune.
	 * 
	 * @return string
	 */
	public function getNom() : string;
	
	/**
	 * Gets the siren of the commune, if any.
	 * 
	 * @return ?string
	 */
	public function getSiren() : ?string;
	
	/**
	 * Gets the code epci of the commune.
	 * 
	 * @return ?string
	 */
	public function getCodeEpci() : ?string;
	
	/**
	 * Gets the codes postaux for this commune.
	 * 
	 * @return array<int, string>
	 */
	public function getCodesPostaux() : array;
	
	/**
	 * Gets the population of this commune.
	 * 
	 * @return ?int
	 */
	public function getPopulation() : ?int;
	
	/**
	 * Gets this interface specifies a geometry GeoJSON object.
	 * 
	 * @return ?GeoJsonGeometryInterface
	 */
	public function getGeometry() : ?GeoJsonGeometryInterface;
	
}
