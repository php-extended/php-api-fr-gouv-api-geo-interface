<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo;

use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Stringable;

/**
 * ApiFrGouvApiGeoApiEndpoint interface file.
 *
 * This represents the geo.api.gouv.fr endpoint.
 *
 * @author Anastaszor
 */
interface ApiFrGouvApiGeoEndpointInterface extends Stringable
{
	
	/**
	 * Gets the list of the regions.
	 *
	 * @return array<integer, ApiFrGouvApiGeoRegionInterface>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRegions() : array;
	
	/**
	 * Gets the region with the given code.
	 *
	 * @param string $codeRegion
	 * @return ApiFrGouvApiGeoRegionInterface
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRegion(string $codeRegion) : ApiFrGouvApiGeoRegionInterface;
	
	/**
	 * Gets the list of departements.
	 *
	 * @return array<integer, ApiFrGouvApiGeoDepartementInterface>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDepartements() : array;
	
	/**
	 * Gets the departement with the given code.
	 *
	 * @param string $codeDepartement
	 * @return ApiFrGouvApiGeoDepartementInterface
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDepartement(string $codeDepartement) : ApiFrGouvApiGeoDepartementInterface;
	
	/**
	 * Gets the departements that are attached to the given region.
	 *
	 * @param string $codeRegion
	 * @return array<integer, ApiFrGouvApiGeoDepartementInterface>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDepartementsFromRegion(string $codeRegion) : array;
	
	/**
	 * Gets the list of communes.
	 * 
	 * @return array<integer, ApiFrGouvApiGeoCommuneInterface>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCommunes() : array;
	
	/**
	 * Gets the communes that are attached to the given departement.
	 *
	 * @param string $codeDepartement
	 * @return array<integer, ApiFrGouvApiGeoCommuneInterface>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCommunesFromDepartement(string $codeDepartement) : array;
	
	
	/**
	 * Gets the commune with the given code.
	 *
	 * @param string $codeCommune
	 * @return ApiFrGouvApiGeoCommuneInterface
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCommune(string $codeCommune) : ApiFrGouvApiGeoCommuneInterface;
	
}
